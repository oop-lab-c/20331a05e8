//Program to demonstrate the creation of threads in java
import java.util.*;
//creating a child class that extends Thread class
class ThreadClassJava extends Thread
{
    public static void main(String[] args)
    {
    //creating an object of the newly created class
    ThreadClassJava obj=new ThreadClassJava();
    System.out.println("Thread about to start");
    obj.start();//calling start() method
    obj.setName("MyThread");//assigning a name to the thread
    System.out.println("Thread name :"+obj.getName());//displaying the thread name returned
    }
    //overriding the run() method with the code that is to be executed by the thread
    public void run()
    {
        System.out.println("Thread is running...");
        for(int i=0;i<10;i++)
        {
        try
        {
            Thread.sleep(1000);//moving the thread to the blocked state till the specifies number of milliseconds
            System.out.println("value : "+i);
        }
        catch(InterruptedException e)
        {
            System.out.println("Thread Interrupted");
        }
        }
    }
}