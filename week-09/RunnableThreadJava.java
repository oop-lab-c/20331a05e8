//Program to demonstrate the creation of threads in java
import java.util.*;
//creating a child class that implements Runnable interface
class RunnableThreadJava implements Runnable
{
    public static void main(String[] args)
    {
    //creating an object of the newly created class
    RunnableThreadJava obj=new RunnableThreadJava();
    //Create the Thread class object by passing above created object as parameter to the Thread class constructor
    Thread thread=new Thread(obj);
    System.out.println("Thread about to start");
    thread.start();//calling start() method on the Thread class object
    thread.setName("MyThread");//assigning a name to the thread
    System.out.println("Thread name :"+thread.getName());//displaying the thread name returned
    }
    //overriding the run() method with the code that is to be executed by the thread
    public void run()
    {
        System.out.println("Thread is running...");
        for(int i=0;i<10;i++)
        {
        try
        {
            Thread.sleep(1000);//moving the thread to the blocked state till the specifies number of milliseconds
            System.out.println("value : "+i);
        }
        catch(InterruptedException e)
        {
            System.out.println("Thread Interrupted");
        }
        }
    }
}