//Java function to check whether a number is even or odd.
import java.util.*;
class EvenOddJava
{
    static void Check(int n)
    {
        if(n%2==0)//condition for even number
            System.out.println(n+" is even");
        else
            System.out.println(n+" is odd");
    }
    public static void main(String[] args)
    {
    System.out.println("Enter any number:");
    Scanner input=new Scanner(System.in);
    int n=input.nextInt();
    Check(n);//Calling the Even function by passing the number as parameter
    }
}