//Java program to compute arithmetic operations with the user input and choice of operation(if..else if ..else).
import java.util.*;
class CalcJava
{
    public static void main(String[] args)
    {
        Scanner input=new Scanner(System.in);//Creating an object to the Scanner class
        System.out.println("enter operand-1:");//Asking the user to enter the operand
        int a=input.nextInt();//Reading the user input from the console 
        System.out.println("enter operand-2:");
        int b=input.nextInt();
        System.out.println("Enter any operator of your choice:(+,-,*,/,%):");
        char op=input.next().charAt(0);//Reading the character input from the user
        if(op=='+')//checking the operator entered
        {
            System.out.println("Arithmetic operation choosen is Addition\n");
            System.out.println("Result is: "+(a+b));//Displaying the Result
        }
        else if(op=='-')
        {
            System.out.println("Arithmetic operation choosen is Subtraction\n");
            System.out.println("Result is: "+(a-b));
        }
        else if(op=='*')
        {
            System.out.println("Arithmetic operation choosen is Multiplication\n");
            System.out.println("Result is: "+(a*b));
        }
        else if(op=='/')
        {
            System.out.println("Arithmetic operation choosen is Division\n");
            if(b==0)
            //If Denominator is zero, Result will be INFINITY
            System.out.println("denominator should not be zero");
            else
            System.out.println("Quotient is: "+((float)a/b));//Type casting(Widening) from int to float
        }
        else if(op=='%')
        {
            System.out.println("Arithmetic operation choosen is Modulo Division\n");
            if(b==0)
            System.out.println("denominator should not be zero");
            else
            System.out.println("Remainder is: "+(a%b));
        }
        //If the operator doesn't match
        else
        {
            System.out.println("Invalid operator");
        }
    }
}