//C++ program to display Hello 'Username'', where 'Username' will be given by the user
#include<iostream>
using namespace std;
int main()
{
    cout<<"Enter your name: "<<endl;//Asking the user to enter his/her name
    string Username;//Declaring a string
    cin>>Username;//Taking username as input from the user
    cout<<"Hello "+Username;//Displaying the output
    return 0;
}