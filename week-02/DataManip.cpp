//C++ program to demonstrate the usage of datamanipulators(endl,ends,ws,flush,setw,setfill,setprecision)
#include<iostream>
#include<iomanip>
#include<istream>
#include<sstream>
#include<string>
using namespace std;
int main()
{
    int a,b;
    string S1,S2;
    float f;
    //endl manipulator to give a new line
    cout<<"hello"<<endl<<"World"<<endl;
    //ends to add a null character
    cout <<"hello"<<ends<<"world"<<endl;;
    //ws to Omit the leading white spaces
    istringstream str("\t\n          Welcome");
    string S;
    getline(str>>ws,S);
    cout<<S<<endl;
    //setw to set the field width 
    //setfill to set the character to be filled
    cout<<"Enter 2 integers : "<<endl;
    cin>>a>>b;
    cout<<"Enter 2 Strings : "<<endl;
    cin>>S1>>S2;
    cout<<S1<<setw(10)<<setfill('*')<<a<<endl<<S2<<setw(5)<<b<< endl;
    cout<<S1<<setw(10)<<left<<setfill('@')<<a<<endl;
    //setprecision to fix the precision
    cout<<"Enter any floating point number : "<<endl;
    cin>>f;
    cout<<setprecision(2)<<f<<endl;
    cout<<setprecision(4)<<f<<flush<<endl;
    //flush to flush the buffer stream
    cout.flush();
    cout<<flush;
    cout<<"Output Buffer cleared";
    return 0;
}