//C++ function to check whether a number is even or odd
#include<iostream>
using namespace std;
int n;//Global declaration of an integer
int Check(int n)//function definition
{
    if(n%2==0)//condition for even number
    return 0;
    else
    return 1;
}
int main()
{
    cout<<"Enter any number:"<<endl;
    cin>>n;//Taking a number as input from the user
    if(Check(n)==0)//calling the function by passing the number as parameter
    {
        cout<<n<<" is Even"<<endl;
    }
    else
    {
        cout<<n<<" is Odd"<<endl;
    }
    return EXIT_SUCCESS;
}