//C++ program to demonstrate default and parameterized constructor and destructor.
#include<iostream>
using namespace std;
class Student
{
    string fName;
    double sPercentage;
    public:
    string collegeName;
    int collegeCode;
    //Assigning the values using default constructor
    Student()
    {
        collegeName="MVGR";
        collegeCode=33;
    }
    //Assigning the values using parameterized constructor
    Student(string fullName,double semPercentage)
    {
        fName=fullName;
        sPercentage=semPercentage;
    }
    //function to display the student details of default constructor
    void display1()
    {
        cout<<"College Name : "<<collegeName<<endl;
        cout<<"College Code : "<<collegeCode<<endl;
    }
    //function to display the student details of parameterized constructor
    void display2()
    {
        cout<<"Full Name : "<<fName<<endl;
        cout<<"Sem Percentage : "<<sPercentage<<endl;
    }
    //destructor
    ~Student()
    {
        cout<<"Object Destroyed"<<endl;
    }
};
int main()
{
    cout<<"Enter your Full Name :";
    string fullName;
    cin>>fullName;
    cout<<"Enter your Sem Percentage : ";
    double semPercentage;
    cin>>semPercentage;
    Student obj1;
    obj1.display1();
    Student obj2(fullName,semPercentage);
    obj2.display2();
    return 0;
}