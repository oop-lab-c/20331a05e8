//C++ program to display area and volume of box using #include, #ifdef and #ifndef 
#include<iostream>//including pre-existing header file
//including user-defined header files
#include"boxArea.h"
#include"boxVolume.h"
using namespace std;
int main()
{
  float l,w,h;
  //taking the Box Dimensions as input from the user
  cout<<"Enter the length of the box: "<<endl;
  cin>>l;
  cout<<"Enter the width of the box: "<<endl;
  cin>>w;
  cout<<"Enter the height of the box: "<<endl;
  cin>>h;
  //Defining length,width,height
  #define length l
  #define width w
  #define height h
  //If lenngth,width,height are defined, the functions can be called
  #ifdef length
  #ifdef width
  #ifdef height
  //calling the functions
  boxArea(length,width);
  boxVolume(length,width,height);
  #endif
  #endif
  #endif
  /*If length,width,height are not defined,
    First , define them,
    And then call the functions*/
  #ifndef length
  #ifndef width
  #ifndef height
  #define length l
  #define width w
  #define height h
  //calling the functions
  boxArea(length,width);
  boxVolume(length,width,height);
  #endif
  #endif
  #endif
  return 0;
}




