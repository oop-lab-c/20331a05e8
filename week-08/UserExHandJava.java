//Program to demonstrate user defined exceptions in java with finally block
import java.util.*;
//A class that represents user-defined exception
class MyException extends Exception
{
}
//A class that uses above exception
public class UserExHandJava
{
    public static void main(String[] args)
    {
        int a,b;
        Scanner input=new Scanner(System.in);
        System.out.println("Enter the Numerator : ");
        a=input.nextInt();
        System.out.println("Enter the Denominator : ");
        b=input.nextInt();
        try{
            int c=a/b;
            if(c==0)
            {
            //Throw an object of user-defined exception
            throw new MyException();
            }
            System.out.println("Result after division : "+c);

        }
        catch(MyException e)
        {
            System.out.println("Exception caught");
            System.out.println("Denominator should be less than the Numerator");
            //Print the message from MyException object
            System.out.println(e.getMessage());
        }
        finally
        {
            System.out.println("Now the finally block is executed ");
        }
    }
}