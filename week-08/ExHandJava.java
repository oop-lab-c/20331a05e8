//Demonstrate built-in exception in Java
import java.util.*;
class ExHandJava
{
    public static void main(String[] args)
    {
        int a,b;
        Scanner input=new Scanner(System.in);
        System.out.println("Enter the Numerator : ");
        a=input.nextInt();
        System.out.println("Enter the Denominator : ");
        b=input.nextInt();
        //code that might throw an exception
        try
        {
            System.out.println("Result after division :"+a/b);
        }
        //block to handle the exception
        catch(ArithmeticException e)
        {
            System.out.println("Exception caught");
            System.out.println("Denominator should not be zero");
        }
        //block that executes whether exception is raised and handled or not
        finally
        {
            System.out.println("Finally block is executed");
            System.out.println("end");
        }
    }
}