//Java program to demonstrate default and parameterized constructor and destructor
import java.util.*;
class Student
{
  String fName,cName;
  int rNum,cCode;
  double sPercentage;
  //Assigning values using parameterized constructor
  Student(String fullName,int rollNum,double semPercentage,String collegeName,int collegeCode)
  {
    fName=fullName;
    rNum=rollNum;
    sPercentage=semPercentage;
    cName=collegeName;
    cCode=collegeCode;
  }
  //function to display the Student details
  void display()
  {
  System.out.println("Name: "+fName);
  System.out.println("Roll number: "+rNum);
  System.out.println("Sem Percentage: "+sPercentage);
  System.out.println("College Name: "+cName);
  System.out.println("College Code: "+cCode);
  }
  //destructor-clean up process before the object is garbage collected
  protected void finalize()
  {
    System.out.println("Object destroyed");
  };
}
class ConstDestJava
{
  public static void main(String[] args)
  {
    Scanner input=new Scanner(System.in);
    //Reading the user input from the console
    System.out.println("Enter your Full Name : ");
    String fullName=input.nextLine();
    System.out.println("Enter your College Name : ");
    String collegeName=input.nextLine();
    System.out.println("Enter your Roll Number : ");
    int rollNum=input.nextInt();
    System.out.println("Enter your Sem Percentage : ");
    double semPercentage=input.nextDouble();
    System.out.println("Enter your College Code : ");
    int collegeCode=input.nextInt();
    Student obj=new Student(fullName,rollNum,semPercentage,collegeName,collegeCode);//object created and parameterized constructor called
    obj.display();
    obj=null;
    System.gc();
  } 
}