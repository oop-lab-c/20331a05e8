//C++ program to demonstrate Simple Inheritence
#include <iostream>
using namespace std;
class Cube //Base class
{
   public:
    int faces = 6;
    void dimension() 
    {
        cout<<"I am Base class"<<endl;
        cout << "I am a 3D object" << endl;
    }
};
// Inheriting a child class from a base class
class Cuboid : public Cube 
{
   public:
    void sides() 
    {
        cout<<"I am inherited from Base class"<<endl;
        cout << "All my sides are not equal" << endl;
    }
};
int main()
{
    Cuboid obj;  
    // An object of child class has all data members and member functions of Base class
    cout<<"No.of faces :"<<obj.faces<<endl;
    obj.dimension();
    obj.sides();
    return 0;
}