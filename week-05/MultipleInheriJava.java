//Java program to demonstrate 'Diamond Problem' using inheritence
import java.util.*;
class GrandParent//Base class
{
    public void display();
}
//Hierarchical Inheritance
class Parent1 extends GrandParent
{
    default void display()
    {
        System.out.println("I am Parent1 interface");
    }
}
class Parent2 extends GrandParent
{
    default void display()
    {
        System.out.println("I am Parent2 Interface");
    }
}
//Multiple Inheritance
class Child extends Parent1,Parent2 
{
    public void display()
    {
        System.out.println("I am GrandParent class");
        Parent1.super.display();
        Parent2.super.display();
    }
}
class MultipleInheriJava
{
    public  static void main(String[] args)
    {
        Child obj=new Child();//Creating an object for child class
        obj.display();
    }
}
