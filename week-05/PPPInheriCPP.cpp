//Program to demonstrate Private, Protected and Public inheritance in c++
#include <iostream>
using namespace std;

class Base 
{
  private:
    int pri=1;
  protected:
    int pro=2;
  public:
    int pub=3;
   //function to access private member
    int getPri()
    {
      return pri;
    }
};

class PublicDerived:public Base 
{
  //pub becomes public
  //pro remains protected
  //pri is inaccessible to the derived class
  //getPri() function of the Base class becomes public
  public:
  //function to access protected member from Base class
    int getPro() 
    {
      return pro;
    }
};

class ProtectedDerived:protected Base
{
  //pro,pub,getPri() are inherited as protected
  //pri is inaccessible to the derived class
  public:
  //function to access protected member from Base
   int getPro()
    {
      return pro;
    }
  //function to access public member from the base
    int getPub() 
    {
      return pub;
    }
};

class PrivateDerived:private Base
{
  //pro,pub,getPri() are inherited as private
  //pri is inaccessible
    public:
    //function to access protected member
    int getPro()
    {
      return pro;
    }
    //function to access private member
    int getPub() 
    {
      return pub;
    }
};

int main() 
{
cout<<"*Public Inheritance*"<<endl;
PublicDerived obj1;
cout<<"Private = "<< obj1.getPri()<<endl;
cout <<"Protected = "<<obj1.getPro()<<endl;
cout<<"Public = "<<obj1.pub<<endl;

cout<<"\n*Protected Inheritance*"<<endl;
ProtectedDerived obj2;
cout<<"Private members cannot be accessed."<<endl;
cout<<"Protected = "<<obj2.getPro()<<endl;
cout<<"Public = "<<obj2.getPub()<<endl;

cout<<"\n*Private Inheritance*"<<endl;
PrivateDerived obj3;
cout<<"Private members cannot be accessed."<<endl;
cout<<"Protected = "<<obj3.getPro()<<endl;
cout<<"Public = "<<obj3.getPub()<<endl;
return 0;
}