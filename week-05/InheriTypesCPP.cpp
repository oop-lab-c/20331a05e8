//C++ program to demonstrate the types of inheritance
#include<iostream>
using namespace std;
//Single Inheritance
class Base1
{
    public:
    Base1()
    {
        cout<<"I am Base1 class"<<endl;
    }
};
class Derived1:Base1
{
    public:
    Derived1()
    {
        cout<<"I am Derived1 class inherited from Base1 class"<<endl;
    }
};
//Multiple Inheritance
class Base2a
{
    public:
    Base2a()
    {
        cout<<"I am Base2a class"<<endl;
    }
};
class Base2b
{
    public:
    Base2b()
    {
        cout<<"I am Base2b class"<<endl;
    }
};
class Derived2:Base2a,Base2b
{
    public:
    Derived2()
    {
        cout<<"I am Derived2 class inherited from Base2a and Base2b classes"<<endl;
    }
};
//Hierarchical Inheritance
class Base3
{
    public:
    Base3()
    {
        cout<<"I am Base3 class"<<endl;
    }
};
class Derived3a: Base3
{
    public:
    Derived3a()
    {
        cout<<"I am Derived3a class inherited from Base3 class"<<endl;
    }
};
class Derived3b: Base3
{
    public:
    Derived3b()
    {
        cout<<"I am Derived3b class inherited from Base3 class"<<endl;
    }
};
//MultiLevel Inheritance
class Base4
{
    public:
    Base4()
    {
        cout<<"I am Base4 class"<<endl;
    }
};
class Derived4a:Base4
{
    public:
    Derived4a()
    {
        cout<<"I am Derived4a class inherited from Base4 class"<<endl;
    }
};
class Derived4b:Derived4a
{
    public:
    Derived4b()
    {
        cout<<"I am Derived4b class inherited from Derived4a class"<<endl;
    }
};
//Hybrid Inheritance
class Base5
{
    public:
    Base5()
    {
        cout<<"I am Base5 class"<<endl;
    }
};
class Derived5a:Base5
{
    public:
    Derived5a()
    {
        cout<<"I am Derived5a class inherited from Base5 class"<<endl;
    }
};
class Derived5a1:Derived5a
{
    public:
    Derived5a1()
    {
        cout<<"I am Derived5a1 class inherited from Derived5a class"<<endl;
    }
};
class Derived5a2:Derived5a
{
    public:
    Derived5a2()
    {
        cout<<"I am Derived5a2 class inherited from Derived5a2 class"<<endl;
    }
};
int main()
{
    cout<<"1) Single Inheritance :"<<endl;
    Derived1 obj1;
    cout<<"2) Multiple Inheritance :"<<endl;
    Derived2 obj2;
    cout<<"3) Hierarchical Inheritance :"<<endl;
    Derived3a obj3a;
    Derived3b obj3b;
    cout<<"4) MultiLevel Inheritance :"<<endl;
    Derived4b obj4b;
    cout<<"5) Hybrid Inheritance :"<<endl;
    Derived5a1 obj5a1;
    Derived5a2 obj5a2;
    return 0;
}