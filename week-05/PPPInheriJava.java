import java.util.*;
class Base1
{
    Base1(){
        System.out.println("Base1 class");
    }
}
class child1 extends Base1
{
    child1(){
        System.out.println("Child1 class");
    }
}
protected class Base2
{
    Base2(){
        System.out.println("Basse2 class");
    }
}
class child2 extends Base2{
    child2(){
        System.out.println("child2 class");
    }
}
private class Base3
{
    Base3(){
        System.out.println("Base3 class");
    }
}
class child3 extends Base3{
    child3(){
        System.out.println("Child3 class");
    }
}
class PPPInheriJava
{
    public static void main(String[] args)
    {
        child1 o1=new child1();
        child2 o2=new child2();
        child3 o3=new child3();
    }
}
