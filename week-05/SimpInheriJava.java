//Java program to demonstrate Simple Inheritence
import java.util.*;
class Cube//Base class
{
   int faces=6;
   void dimension()
   {
       System.out.println("I am Base class");
       System.out.println("I am a 3D object");
   }
}
// Inheriting a child class from a base class
class Cuboid extends Cube
{
    void sides()
    {
        System.out.println("I am derived from Base class");
        System.out.println("All my sides are not equal");
    }
}
class SimpInheriJava
{
    public static void main(String[] args)
    {
    Cuboid obj=new Cuboid();
    // An object of child class has all data members and member functions of Base class
    System.out.println("No. of faces : "+obj.faces);
    obj.dimension();
    obj.sides();
    }
}