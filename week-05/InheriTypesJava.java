//Java program to demonstrate the types of inheritance
import java.util.*;
//Sinle Inheritance
class Base1
{
    Base1()
    {
        System.out.println("This is Base1 class");
    }
}
class Derived1 extends Base1
{
    Derived1()
    {
        System.out.println("This is Derived1 class inherited from Base1 class");
    }
}
//Hierarchical Inheritance
class Base2
{
    Base2()
    {
        System.out.println("This is Base2 class");
    }
}
class Derived2a extends Base2
{
    Derived2a()
    {
        System.out.println("This is Derived2a class inherited from Base2 class");
    }
}
class Derived2b extends Base2
{
    Derived2b()
    {
        System.out.println("This is Derived2b class inherited from Base2 class");
    }
}
//MultiLevel Inheritance
class Base3
{
    Base3()
    {
        System.out.println("This is Base3 class");
    }
}
class Derived3a extends Base3
{
    Derived3a()
    {
        System.out.println("This is a Derived3a class inherited from Base3 class");
    }
}
class Derived3b extends Derived3a
{
    Derived3b()
    {
        System.out.println("This is a Derived3b class inherited from Derived3a class");
    }
}
//Multiple Inheritance
interface Base4a
{
    public void Base4a();
}
interface Base4b
{
    public void Base4b();
}
class Derived4 implements Base4a, Base4b
{
    public void Base4a()
    {
        System.out.println("This is a Base4a interface");
    }
    public void Base4b()
    {
        System.out.println("This is a Base4b interface");
    }
    Derived4()
    {
        System.out.println("This is a Derived4 class implemented from Base4a and Base4b interfaces");
    }
}
//Hybrid Inheritance
class Base5
{
    Base5()
    {
        System.out.println("This is Base5 class");
    }
}
class Derived5a extends Base5
{
    Derived5a()
    {
        System.out.println("This is a Derived5a class inherited from Base5 class");
    }
}
class Derived5a1 extends Derived5a
{
    Derived5a1()
    {
        System.out.println("Thsi is a Derived5a1 class inherited from Derived5a class");
    }
}
class Derived5a2 extends Derived5a
{
    Derived5a2()
    {
        System.out.println("This is a Derived5a2 class inherited from Derived5a class");
    }
}
class InheriTypesJava
{
    public static void main(String[] args)
    {
        System.out.println("1) Single Inheritance :");
        Derived1 obj1=new Derived1();
        System.out.println("2) Hierarchical Inheritance :");
        Derived2a obj2a=new Derived2a();
        Derived2b obj2b=new Derived2b();
        System.out.println("3) MultiLevel Inheritance :");
        Derived3b obj3b=new Derived3b();
        System.out.println("4) Multiple Inheritance :");
        Derived4 obj4=new Derived4();
        obj4.Base4a();
        obj4.Base4b();
        System.out.println("5) Hybrid Inheritance :");
        Derived5a1 obj5a1=new Derived5a1();
        Derived5a2 obj5a2=new Derived5a2();
    }
}