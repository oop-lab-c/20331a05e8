//c++ program to demonstrate method/function overlaoding
#include<iostream>
using namespace std;
class Demo
{
    public:
    //Based on number of parameters
    void fun()
    {
        cout<<"Hello World"<<endl;
    }
    void fun(int i1)
    {
        cout<<"Displaying an integer :"<<i1<<endl;
    }
    void fun(string str1)
    {
        cout<<"Displaying a string :"<<str1<<endl;
    }
    void fun(float f1)
    {
        cout<<"Displaying a floating point number :"<<f1<<endl;
    }
    void fun(int i1,string str1)
    {
        cout<<"Concatenation of an integer and a string :"<<i1<<str1<<endl;
    }

    //Based on data type of the parameters
    void fun(int i1,int i2)
    {
        cout<<"Addition of two integers :"<<i1+i2<<endl;
    }
    void fun(string str1,string str2)
    {
        cout<<"Concatenation of two strings :"<<str1+str2<<endl;
    }
    void fun(float f1,float f2)
    {
        cout<<"Addition of two floating point numbers :"<<f1+f2<<endl;
    }

    //Based on sequence of data type of parameters
    void fun(int i1,float f1)
    {
        cout<<"Addition of an integer and floating point number :"<<i1+f1<<endl;
    }
    void fun(float f1, int i1)
    {
        cout<<"Subtraction of an integer from floating point number :"<<f1-i1<<endl;
    }
};
int main()
{
    int i1,i2;
    string str1,str2;
    float f1,f2;
    Demo obj;
    cout<<"Enter an integer :"<<endl;
    cin>>i1;
    cout<<"Enter another integer :"<<endl;
    cin>>i2;
    cout<<"Enter any string :"<<endl;
    cin>>str1;
    cout<<"Enter another string :"<<endl;
    cin>>str2;
    cout<<"Enter a floating point number :"<<endl;
    cin>>f1;
    cout<<"Enter another floating point number :"<<endl;
    cin>>f2;
    obj.fun();
    obj.fun(i1);
    obj.fun(str1);
    obj.fun(f1);
    obj.fun(i1,str1);
    obj.fun(i1,i2);
    obj.fun(str1,str2);
    obj.fun(f1,f2);
    obj.fun(i1,f1);
    obj.fun(f1,i1);
    return 0;
}