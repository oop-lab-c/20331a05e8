//Program to demonstrate pure abstraction using interfaces in Java
import java.util.*;
interface Animal
{
    //Abstract methods of interface
    public void Bark();
    public void Walk();
}
class Dog implements Animal
{
    //Implementation of abstract methods in subclass
    public void Bark()
    {
        System.out.println("Bow");
    }
    public void Walk()
    {
        System.out.println("Walking");
    }
}
class PureAbsJava
{
    public static void main(String[] args)
    {
        Dog obj=new Dog();
        obj.Bark();
        obj.Walk();
    }
}