//Program to demonstrate Partial Abstraction in Java
import java.util.*;
abstract class Animal
{
    //abstract method
    abstract void Bark();
    //Non-abstract method
    void Walk()
    {
        System.out.println("Walking");
    }
}
class Dog extends Animal
{
    //implementation of abstract method in derived class
    void Bark()
    {
        System.out.println("Bow");
    }
}
class ParAbsJava
{
    public static void main(String[] args)
    {
        Dog obj=new Dog();
        obj.Bark();
        obj.Walk();
    }
}