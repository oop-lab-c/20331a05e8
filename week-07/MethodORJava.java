//Program to demonstrate method/function overriding in java
import java.util.*;
//Base class
class Shape
{
    int sides()
    {
        return 0;
    }
}
class Square extends Shape
{
    //This method overrides sides() of the class Shape
    int sides()
    {
        return 4;
    }
}
class Triangle extends Shape
{
    //This method overrides sides() of the class Shape
    int sides()
    {
        return 3;
    }
}
class Pentagon extends Shape
{
    //This method overrides sides() of the class Shape
    int sides()
    {
        return 5;
    }
}
//class to create objects and call the methods
class MethodORJava
{
    public static void main(String[] args)
    {
        //Method of Base class is called
        Shape obj=new Shape();
        System.out.println("Number of sides of a circle : "+obj.sides());
        //Methods of derived classes are called
        //Method in derived classes overrides the method in Base class
        Square obj1=new Square();
        System.out.println("Number of sides of a Square : "+obj1.sides());
        Triangle obj2=new Triangle();
        System.out.println("Number of sides of a Triangle : "+obj2.sides());
        Pentagon obj3=new Pentagon();
        System.out.println("Number of sides of a Pentagon : "+obj3.sides());
    }
}