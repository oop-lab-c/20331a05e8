//Demonstrate a event registration form using AWT
import java.awt.*;
import java.awt.event.*;
class AWTJava extends Frame{
    TextField t1,t2,t3,t4;
    Label l5;
    AWTJava()
    {
        setLayout(new FlowLayout());
        this.setLayout(null);
        Label l1=new Label("Name",Label.CENTER);
        t1=new TextField(30);
        Label l2=new Label("Email-id",Label.CENTER);
        t2=new TextField(30);
        Label l3=new Label("Regd number",Label.CENTER);
        t3=new TextField(30);
        Label l4=new Label("Password",Label.CENTER);
        t4=new TextField(30);
        Button b1=new Button("Submit");
        l5=new Label(" ");
        this.add(l1);
        this.add(l2);
        this.add(l3);
        this.add(l4);
        this.add(t1);
        this.add(t2);
        this.add(t3);
        this.add(t4);
        this.add(b1);
        this.add(l5);
        l1.setBounds(70,90,90,60);
        l2.setBounds(70,130,90,60);
        l3.setBounds(70,170,90,60);
        l4.setBounds(70,210,90,60);
        t1.setBounds(200,100,90,20);
        t2.setBounds(200,140,90,20);
        t3.setBounds(200,180,90,20);
        t4.setBounds(200,220,90,20);
        b1.setBounds(100,300,70,40);
        l5.setBounds(200,300,500,20);
        b1.addActionListener(new ActionListener(){
    public void actionPerformed(ActionEvent e)
    {
        String name=t1.getText(),id=t2.getText(),regdNo=t3.getText(),password=t4.getText();
        //checking the validity of name
        char ch[]= name.toCharArray();
            for(char i : ch)
            {
                if(Character.isDigit(i))
                {
                    l5.setText("Enter a valid name!!!\nName shouldn't contain number");
                    break;
                }
            }
        //checking the validity of email-id
        if(id.contains("@")==false)
        {
            l5.setText("\nYour mail id must have @!!!");
        }
        //checking the validity of registration number
        if((regdNo.length())!=10)
        {
            l5.setText("length of the registration Number should be 10!!!");
        }
        //checking the validity of password
        //checking whether the password length is between 6-15 or not
        if((password.length()<6)||(password.length()>15))
        {
            l5.setText("Length of the password must be 6 to 15 characters");
        }
        //checking whether the password contain atleast one digit or not
        char c[]= password.toCharArray();
        for(char j : c)
        {
            int count=0;
            if(Character.isDigit(j)==false)
            {
               count=count+1;
            }
            if(count==password.length())
            {
                l5.setText("Password should contain digits");
                break;
            }
        }
        //checking whether the password contains atleast 1 uppercase letter or not
        if(true)
        {
            int count1=0;
            for(int i=65;i<=90;i++)
            {
                char c1=(char)i;
                String s1=Character.toString(c1);
                if(password.contains(s1))
                {
                    count1=1;
                }
            } 
            if(count1==0)
            {
                l5.setText("Password should contain atleast 1 uppercase letter");
            }
        }
        //checking whether the password contains atleast one lowercase letter or not
        if(true)
        {
            int count2=0;
            for(int i=90;i<=122;i++)
            {
                char c2=(char)i;
                String s2=Character.toString(c2);
                if(password.contains(s2))
                {
                    count2=1;
                }
            } 
            if(count2==0)
            {
                l5.setText("Password should contain atleast 1 lowercase letter");
            }
        }
        //checking whether the password contains atleast one special character or not
        String specialCharacterString="~!@#$%^&*()_+-={}[]|\\:<>?;',./'";
        int count3=0;
        for(int i=0;i<password.length();i++)
        {
            char c3=password.charAt(i);
            if(specialCharacterString.contains(Character.toString(c3))==false);
            {
                count3=count3+1;
            }
            if(count3==0)
            {
                
                l5.setText("Password should contain atleast 1 special character");
            }
        }
        //if all the requirements satisfied, then display that you are successfully registered
        if(l5.getText()==" ")
        {
            l5.setText("Successfully registered");
        }
    }
    });
    }
    public static void main(String[] args)
    {
        AWTJava obj=new AWTJava();
        obj.setVisible(true);
        obj.setSize(400,400);
        obj.setTitle("REGISTRATION FORM");
    }
}
